// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
/*****************************************************************************
 * File:        iPlayer/iAppCtrl/plugins/iPlugChromium.c
 * Description: Manage display of Webpages with Chromium
 * Author:      Marc Simonetti <marc.simonetti@ipreso.com>
 * Changes:
 *  - 2010.02.05: Original revision
 *  - 2016.10.15: Updates for Debian Stretch
 *****************************************************************************/

#include "iPlugChromium.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>
#include <dirent.h>

#include "log.h"
#include "iCommon.h"
#include "apps.h"
#include "SHM.h"

int gOurStop;

int initPlugin (sPlugins * plugins, void * handle)
{
    int i = 0;

    iDebug ("[%s] initialization", PLUGINNAME);
    while (i < PLUGIN_MAXNUMBER && strlen ((*plugins)[i].plugin))
        i++;

    if (i >= PLUGIN_MAXNUMBER)
    {
        iError ("Maximum number of Plugins has been reached");
        return (0);
    }

    strncpy ((*plugins)[i].plugin, PLUGINNAME, PLUGIN_MAXLENGTH-1);

    (*plugins)[i].handle     = handle;
    (*plugins)[i].pGetCmd    = dlsym (handle, "getCmd");
    (*plugins)[i].pPrepare   = dlsym (handle, "prepare");
    (*plugins)[i].pPlay      = dlsym (handle, "play");
    (*plugins)[i].pStop      = dlsym (handle, "stop");
    (*plugins)[i].pClean     = dlsym (handle, "clean");

    return (1);
}

char * getCmd (sConfig * config,
               char * zone, char * media, 
               int width, int height, 
               char * buffer, int size)
{
    char value [URL_MAXLENGTH];
    char url [URL_MAXLENGTH];
    char rdm [16];

    iDebug ("[%s] get command", PLUGINNAME);

    // Get the URL to load
    memset (value, '\0', sizeof (value));
    memset (url, '\0', sizeof (url));
    // Get the "url" parameter
    if (getParam (config, media, "url", value, sizeof (value)))
    {
        strncpy (url, value, strlen (value));
    }
    else
    {
        iError ("[%s] Unable to get requested URL", zone);
        return (NULL);
    }

    // Get the "rdm" parameter, to distinguish same URL in same zone
    memset (value, '\0', sizeof (value));
    memset (rdm, '\0', sizeof (rdm));
    if (getParam (config, media, "rdm", value, sizeof (value)))
    {
        strncpy (rdm, value, sizeof (rdm)-1);
    }
    else
    {
        iError ("[%s] No random identifier for Webbrowser '%s'", zone, url);
        return (NULL);
    }

    // Create the command line
    memset (buffer, '\0', size);
    snprintf (buffer, size - 1, 
                "%s "\
                    "--temp-profile "\
                    "--disable-translate "\
                    "--site-per-process "\
                    "--window-size=\"%d,%d\" "\
                    "--user-data-dir=\"%s/%s/%s-%s\" "\
                    "--app=\"%s\" &",
                PLUGINBINARY,
                width, height,
                config->path_tmp, PLUGINNAME, zone, rdm,
                url);
    iDebug ("[Webbrowser] Command: '%s'", buffer);

    return (buffer);
}

int prepare (sConfig * config, int wid)
{
    sApp app;
    sZone zone;
    int screenWidth, screenHeight;
    char buffer [CMDLINE_MAXLENGTH];
    char value [URL_MAXLENGTH];
    char url [URL_MAXLENGTH];

    iDebug ("[%s] preparation for WID %d", PLUGINNAME, wid);

    if (!shm_getApp (wid, &app))
    {
        iError ("[%s] Cannot get application information to prepare it.",
                app.zone);
        return (0);
    }
    
    // Set the correct position for the browser
    if (!shm_getZone (app.zone, &zone))
        return (0);
    if (!getResolution (config, &screenWidth, &screenHeight))
        return (0);

    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -m -1,%d,%d,%d,%d",
                config->path_iwm,
                config->filename,
                wid,
                zone.x * screenWidth / 100,
                zone.y * screenHeight / 100,
                zone.width * screenWidth / 100,
                zone.height * screenHeight / 100);
iDebug ("%s", buffer);
    system (buffer);

    // Hiding the window
/*
    memset (buffer, '\0', CMDLINE_MAXLENGTH);
    snprintf (buffer, CMDLINE_MAXLENGTH-1,
                "%s -z '%s' -w %d -i0",
                config->path_iwm,
                config->filename,
                wid);
iDebug ("%s", buffer);
    system (buffer);
*/

    // Get the URL to load
    memset (url, '\0', sizeof (url));
    // Get the "url" parameter
    if (getParam (config, app.item, "url", value, sizeof (value)))
    {
        strncpy (url, value, strlen (value));
    }
    else
    {
        iError ("[%s] Unable to get requested URL", zone);
        return (0);
    }
    
    // Prepare the command to be sent
    memset (buffer, '\0', sizeof (buffer));
    if (strlen (buffer))
    {
        iDebug ("[Webbrowser] Prepare: %s", buffer);
        system (buffer);
    }

    return (1);
}

int play (sConfig * config, int wid)
{
    sApp app;
    sZone zone;
    int duration;
    char param [32];
    int status;
    char value [URL_MAXLENGTH];
    char url [URL_MAXLENGTH];
    //char buffer [CMDLINE_MAXLENGTH];
    int screenWidth, screenHeight;

    iDebug ("[%s] play for WID %d", PLUGINNAME, wid);

    memset (param, '\0', sizeof (param));
    memset (value, '\0', sizeof (value));

    if (!shm_getApp (wid, &app))
        return (0);

    if (!shm_getZone (app.zone, &zone))
        return (0);

    if (!getResolution (config, &screenWidth, &screenHeight))
        return (0);
    
    // Get the URL to load
    memset (url, '\0', sizeof (url));
    // Get the "url" parameter
    if (getParam (config, app.item, "url", value, sizeof (value)))
    {
        strncpy (url, value, strlen (value));
    }
    else
    {
        iError ("[%s] Unable to get requested URL", app.zone);
        return (0);
    }

    // Get the "duration=X" parameter
    if (!getParam (config, app.item, "duration", value, sizeof (value)))
        duration = 30;
    else
        duration = atoi (value);

    if (duration > 0)
        sleep (duration);
    else
    {
        while ((status = sleep (5)) == 0) { }
    }

    return (1);
}

int stop (sConfig * config, int wid)
{
    sApp app;
    char rdm [16];
    char profile_dir [FILENAME_MAXLENGTH];
    char buffer [CMDLINE_MAXLENGTH];

    iDebug ("[%s] stop for WID %d", PLUGINNAME, wid);

    memset (rdm, '\0', sizeof (rdm));
    memset (profile_dir, '\0', sizeof (profile_dir));
    memset (buffer, '\0', sizeof (buffer));

    if (!shm_getApp (wid, &app))
    {
        iError ("[%s] Cannot get application information to stop it.",
                app.zone);
        return (0);
    }

    // Random parameter
    if (!getParam (config, app.item, "rdm", rdm, sizeof (rdm)))
    {
        iError ("[%s] No random identifier for Webbrowser (WID %d)", app.zone, wid);
        return (0);
    }

    // Remove profile directory
    memset (profile_dir, '\0', sizeof (profile_dir));
    snprintf (profile_dir,
                sizeof (profile_dir) - 1,
                "%s/%s/%s-%s",
                config->path_tmp, PLUGINNAME, app.zone, rdm);
    snprintf (buffer, sizeof (buffer) - 1,
                "rm -rf '%s' 2>&1 | tee /tmp/debug", profile_dir);
    iDebug ("%s", buffer);
    system (buffer);

    return (0);
}

int clean ()
{
    iDebug ("[%s] Killing all instances of %s", PLUGINNAME, PLUGINBINARY);

    system ("pkill chromium");
    return (1);
}
